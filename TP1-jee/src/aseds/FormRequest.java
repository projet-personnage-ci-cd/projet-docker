package aseds;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import aseds.Personne;

/**
 * Servlet implementation class FormRequest
 */
public class FormRequest extends HttpServlet {
	private static final long serialVersionUID = 1L;
    public static final String VUE = "/form/index.jsp";
 
    public FormRequest() {
    	super();
    }
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.getServletContext().getRequestDispatcher( VUE ).forward( request, response );
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String name =request.getParameter("name");
		String characteristics =request.getParameter("phone");
		String photo = request.getParameter("image");
		ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("classpath:applicationContext.xml");	

		PersonneDaoImpl personnejpaDao = ctx.getBean(PersonneDaoImpl.class);
		personnejpaDao.persist(createPersonne(personnejpaDao,name,photo,characteristics));
		

		ctx.close();
		response.sendRedirect("http://localhost:8081/TP1-jee/display");  


//        this.getServletContext().getRequestDispatcher( "/display/data-table.jsp" ).forward( request, response );
	}
	
	
	private static Personne createPersonne(PersonneDaoImpl jpaDao, String name, String photo, String characteristics) {
		Personne prs = new Personne(name, photo, characteristics);
		return prs;
	}

	//${applicationScope.List.Liste} 				   ${applicationScope['List'].getListe(�}

}
