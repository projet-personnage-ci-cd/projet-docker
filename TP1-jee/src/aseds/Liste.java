package aseds;

import java.util.List;

import aseds.Personne;


public class Liste {

	private List<Personne> Liste;
	public Liste() {}
		
	public Liste(List<Personne> Liste) {
		super();
		this.Liste = Liste;
	}

	public void add(Personne personne) {
		Liste.add(personne);
//		return personne.getName();
	}
	
	
	public boolean removeAll() {
		return Liste.removeAll(Liste);

	}

	public List<Personne> getListe() {
		return Liste;
	}

	public void setListe(List<Personne> liste) {
		Liste = liste;
	}
}
