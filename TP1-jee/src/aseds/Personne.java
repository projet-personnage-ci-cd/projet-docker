package aseds;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name="personne")
public class Personne {
    
	@Id
	@Column(name="idPersonne")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private long idPersonne;
	
	private String name;
	private String photo;
	private String characteristics;
	

	public Personne() {}
	

	public Personne(String name, String photo, String characteristics) {
		super();
		this.name = name;
		this.photo = photo;
		this.characteristics = characteristics;
	}


	public long getIdPersonne() {
		return idPersonne;
	}


	public void setIdPersonne(long idPersonne) {
		this.idPersonne = idPersonne;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getPhoto() {
		return photo;
	}


	public void setPhoto(String photo) {
		this.photo = photo;
	}


	public String getCharacteristics() {
		return characteristics;
	}


	public void setCharacteristics(String characteristics) {
		this.characteristics = characteristics;
	}



	
}
