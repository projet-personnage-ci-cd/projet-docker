package aseds;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import aseds.Liste;
import aseds.Personne;

/**
 * Servlet implementation class TableRequest
 */
@WebServlet("/TableRequest")
public class TableRequest extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TableRequest() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("classpath:applicationContext.xml");	
		PersonneDaoImpl personnejpaDao = ctx.getBean(PersonneDaoImpl.class);
		List<Personne> personnelist=personnejpaDao.findAll();
		
		
		request.setAttribute("personnelist", personnelist);

		ctx.close();

		this.getServletContext().getRequestDispatcher("/display/data-table.jsp").forward( request, response );
	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String name=request.getParameter("searchName");
		System.out.println(name);
		ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("classpath:applicationContext.xml");	
		PersonneDaoImpl personnejpaDao = ctx.getBean(PersonneDaoImpl.class);
		List<Personne> personnelist=personnejpaDao.findByCriteria(name);
		request.setAttribute("personnelist", personnelist);
		ctx.close();

	
        this.getServletContext().getRequestDispatcher( "/display/data-table.jsp" ).forward( request, response );
	}

}
