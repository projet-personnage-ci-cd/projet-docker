package aseds;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import aseds.Personne;

@Repository
public class PersonneDaoImpl {

	@PersistenceContext
	private EntityManager em;

	@Transactional
	public void persist(Personne personne) {
		em.persist(personne);
	}
	
	@Transactional(readOnly = true)
	public <T> T find(Class<T> cls, Object id) {
		return em.find(cls, id);
	}

	@Transactional(readOnly = true)
	public List<Personne> findAll() {
		return em.createQuery("SELECT p FROM personne p").getResultList();
	}
	
	@Transactional(readOnly = true)
	public List<Personne> findByCriteria(String filterValue) {
		return em.createQuery("SELECT p FROM Personne p WHERE p.name = :value").setParameter("value",filterValue).getResultList();
	}


	
	
	
}