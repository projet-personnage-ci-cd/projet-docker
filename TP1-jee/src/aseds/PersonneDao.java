package aseds;
import java.util.List;

public interface PersonneDao {
	
	public void persist(Personne personne);
	public <T> T find(Class<T> cls, Object id);
	public List<Personne> findAll();
	public List<Personne> findByCriteria(String filterValue);

	
}