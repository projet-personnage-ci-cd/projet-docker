FROM tomcat
LABEL MAINTAINER Atif <kimco1998@hotmail.fr>
COPY TP1-jee.war $CATALINA_HOME/webapps/
EXPOSE 8080
CMD ["catalina.sh","run"]